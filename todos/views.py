from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todolist_object": todo_lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_details = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_detail": todo_details,
    }
    return render(request, "todos/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_edit)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=todo_edit)
    context = {
        "todolist_edit": todo_edit,
        "form": form
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
        }
    return render(request, "todos/item_create.html", context)

def todo_item_update(request, id):
    todo_update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_update)
        if form.is_valid():
            item = form.save()
            return redirect("to_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_update)
    context = {
        "todoitem_update": todo_update,
        "form": form
    }
    return render(request, "todos/items_update.html", context)
